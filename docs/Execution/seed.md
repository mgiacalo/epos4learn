---
sidebar_position: 3
---

# Set random seeds

By default, the original script to run EPOS4 uses a fixed seed for the random number initialisation, so running the code several times will produce identical results. To change this, you can replace "seedj=111111111" and "seedi=222222222" in the script by
```
seedj=`date '+%N'`
seedi=`date '+%N'`
```
allowing to generate multiple runs and increase the statistics.  
A simpler way to customise the script is to copy it in your folder and run it from there without editing the one delivered by the EPOS developers. The version uploaded **[HERE](./assets/epos.sh)**[^1] contains, for instance, the two flags `-si $N` and `-sj $N` to manually edit the seeds during the generation, which are by default random. 

[^1]: In case of execution issues, simply remove the .sh extension of the script.
