---
sidebar_position: 1
---

# ROOT6

The installation of ROOT is quite simple and a full tutorial can be found in the official [ROOT website](https://root.cern/install/).
Remember to install all the dependencies for your operating system before proceding to the setup of ROOT.

After the installation, make sure that the ROOTSYS variable is set in your bash (it is done automatically when you source the file thisroot.sh).