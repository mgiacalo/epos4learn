---
sidebar_position: 1
---

# Introduction

This tutorial will teach you how to install properly all the packages required to run EPOS4 with all the 
examples provided by the developers of the generator. For the tutorial, Ubuntu 20.04 (including WSL2) has been used, 
but similar commands can be adapted to other operating systems. More documentation can be found in the [EPOS4 official website](https://klaus.pages.in2p3.fr/epos4/), through the manuals, that are also uploaded in this website[^1], and the alphabetic list of commands[^2] to inspect more advanced features.  
As of 22/11/2022, **the compilation of EPOS fails on Apple** machines (error due to fortran using large static arrays). An email has been sent to the developers on the matter.

This tutorial won't help you understand the physics behind the model, but it will simply guide you on how to run your first simulations. An in-depth description of the theoretical basis of the generator is provided in **[this file](./asset/KlausWernerWorkshop2022EPOS4.pdf)**, which comes from a MC Workshop done by Klaus Werner in July 2022.

**29/03/2023 Update:** the website has been updated including the manuals released in the official EPOS4 website, which gave more specific information on the commands available inside the framework.

**Enjoy!**


[^1]: Input/output basics:
<ul>
    <li><a target="\_blank" href={require('./asset/inputOutputFiles.pdf').default}> Input and Output files </a></li>
    <li><a target="\_blank" href={require('./asset/defineReactionAndOuputs.pdf').default}>Reaction and output format definition</a></li> 
    <li><a target="\_blank" href={require('./asset/structureCheckFile.pdf').default}>Structure of CHECK file</a></li>
    <li><a target="\_blank" href={require('./asset/structureRootFile.pdf').default}>Structure of ROOT file</a></li> 
</ul>

Complete Examples:
<ul>
    <li><a target="\_blank" href={require('./asset/stringHadronization.pdf').default}>String Hadronisation</a></li> 
    <li><a target="\_blank" href={require('./asset/electronPositronAnnihilation.pdf').default}>e<sup>+</sup>-e<sup>-</sup> Annihilation</a></li> 
    <li><a target="\_blank" href={require('./asset/protonProtonScattering.pdf').default}>pp scattering</a></li> 
    <li><a target="\_blank" href={require('./asset/leadLeadScattering.pdf').default}>Pb-Pb scattering</a></li> 
</ul>

Simple analysis features and histograms making:
<ul>
    <li><a target="\_blank" href={require('./asset/generalAnalysisSyntax.pdf').default}>General analysis syntax</a></li>  
    <li><a target="\_blank" href={require('./asset/rapidityAndPtDistributionsInStringHadronisation.pdf').default}>p<sub>T</sub> and rapidity distributions in string hadronisation</a></li>
    <li><a target="\_blank" href={require('./asset/distributionsInElectronPositronAnnihilation.pdf').default}>Multiplicity and rapidity distributions in e<sup>+</sup>-e<sup>-</sup> annihilation</a></li>    
    <li><a target="\_blank" href={require('./asset/ptDistributionInProtonProtonScattering.pdf').default}>p<sub>T</sub> distribution in pp scattering</a></li>
</ul>  

[^2]: [Alphabetic list of commands](./asset/alphabeticListOfCommands.pdf). 