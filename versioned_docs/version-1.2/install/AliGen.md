---
sidebar_position: 4
---

# Extra: EPOS4 implementation in AliGenerators

From the AliGenerators tag of 06/12/2022 (VO_ALICE@AliGenerators::v20221206-1), EPOS4 is implemented directly inside the ALICE
framework and can be used on LXPLUS without issues. The installation is coexistent with the previous versions of EPOS,
which can still be executed (for example via CRMC).  
In addition, EPOS4 can be loaded as a standalone module via the command 
```
alienv enter VO_ALICE@EPOS4::v4.0.0-alice1-1
```
which corresponds to the ALICE compatible version of the generator in the framework.  
If you perform an installation of AliGenerators/EPOS4 on your machine, you should be able to use the script `epos` located in the `scripts` folder where the software is installed (path available in `$EPO4`), 
and you can follow exactly the same procedures explained in the end of the **[generation section](../Execution/Start)** by replacing the `$EPO` variable
with `$EPO4`, without exporting the variables shown at the beginning (they are already taken care of by the ALICE framework).  
**INSTEAD** if you are using EPOS4 on **LXPLUS**, you need to copy the script inside one of your folders before being able to work with it, otherwise the permissions of the 
server won't allow you to start the generation. I thought of editing this, but for security reasons on lxplus, it's better to follow the procedure in the next lines.  
After choosing a path (which I suggest to be on your cernbox), and loading the environment, digit the command: 
```
cp $EPO4/scripts/epos .
```
to copy the script inside the selected folder. You should now be able to run the simulation by using a .optns file as described in the following sections.  
As a check, copy one of the examples from EPOS4 in the same folder 
```
cp $EPO4/examples/expl4.optns .
```
and launch your first simulation via
```
./epos expl4
```
to test that everything is working properly. If the "permission denied" error is displayed, try to change the permissions of the script through
```
chmod +x epos
```
which should execute correctly if you put the file in a folder you own. Afterwards, try again to start the simulation.  
This operation is already performed during the building of the software for the `$EPO4/scripts/epos` file, whose owner in LXPLUS is `cvmfs` and not your user, so you won't be able to execute it.
However, the permissions are already set, so putting the file in your domain will make it work (at least from the tests performed).  
If you want to obtain a root file, remember to run the program using the flag `-root`, but I suggest you to first read the **[following sections](../Execution/Syntax.md)**
to understand better the machinery behind the option files and the different parameters that you can enable.