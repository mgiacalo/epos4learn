---
sidebar_position: 5
---

# Extra: Commands with Linux C-shell for EPOS4 setup

**This part is largely untested**, so use it with care. It's vastely taken from the official **[EPOS4 website](https://klaus.pages.in2p3.fr/epos4/code/install)**, and the text was cloned 
from the **[EPOS4 installation page](./EPOS4.md)** of this tutorial.

The first step will be performed using the DEMO mode of EPOS4, in order to test the files and run the first example of the generator (it allows simple applications to be run and does not depend on external libraries).  
CMake3 is required for the installation of the software.  
We need to declare a series of variables, as described by the developers, in which the version of the software is defined in `EPOSVSN`, and the working folder will be `MYDIR`
```
setenv EPOVSN 4.0.0
setenv MYDIR $PWD/
setenv EPO ${MYDIR}epos${EPOVSN}/
setenv LIBDIR ${EPO}bin
setenv OPT ./
setenv HTO ./
setenv CHK ./
```
where the folders OPT, HTO and CHK are respectively for input, histograms and outputs of the generator. 
Now it's time to install the software, so `cd $MYDIR`, download **[here](https://box.in2p3.fr/index.php/s/eLK2fj6mxQC6BRN/download/epos4.0.0.tgz)** the EPOS4 software from the official website. Then extract the content of the downloaded archive `tar xvfz epos${EPOSVSN}.tgz`.

## Compilation and test using DEMO mode

We are going to set the DEMO mode and compile the program without requiring ROOT and FastJet. 

```
setenv COP DEMO
cd $EPO
rm -Rf $LIBDIR && cmake -B$LIBDIR && make -C$LIBDIR -j8
```
Now we can simply run the first example provided by the developers
```
cd examples 
$EPO/scripts/epos expl1 
```

## Compilation and run using the BASIC mode

Now it's time to compile the full program with the BASIC mode and be able to run it fully using ROOT and FastJet.  
It is very important that the variables ROOTSYS, FASTSYS and FASTJETSYS are declared in the bash session, otherwise the compilation will fail (issues in Xepos). Furthermore, the environment variables declared **[here](#compilation-and-test-using-demo-mode)**, in the first code box, are mandatory also for this section. 
```
set path= ( $ROOTSYS/bin $FASTSYS/bin $path )
setenv COP BASIC
setenv LD_LIBRARY_PATH $ROOTSYS/lib:$ROOTSYS/lib/root:/usr/local/lib
cd $EPO
rm -Rf $LIBDIR && cmake -B$LIBDIR && make -C$LIBDIR -j8
``` 
If the instructions were followed properly, you should now be able to run all the other examples provided by the developers (expl 2..6).
```
cd examples 
$EPO/scripts/epos expl5
```

In order to obtain trees from EPOS, the program needs to be run with a specific flag and an enabled function in the .optns `fillTree4(C1)`
```
$EPO/scripts/epos -root expl2
```