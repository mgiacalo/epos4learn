---
sidebar_position: 3
---

# EPOS4

The following instructions were largely taken from the official **[EPOS4](https://klaus.pages.in2p3.fr/epos4/)** website, but they were adapted 
to be executed in a Linux shell (for untested C-shell commands have a look **[here](./LinuxCshell.md)**).  
The first step will be performed using the DEMO mode of EPOS4, in order to test the files and run the first example of the generator (it allows simple applications to be run and does not depend on external libraries).  
CMake3 is required for the installation of the software.  
We need to declare a series of variables, as described by the developers, in which the version of the software is defined in `EPOSVSN`, and the working folder will be `MYDIR`
```
export EPOSVSN=4.0.0
export MYDIR=$PWD/
export EPO=${MYDIR}epos${EPOSVSN}/
export LIBDIR=${EPO}bin
export OPT=./   
export HTO=./
export CHK=./
```
where the folders OPT, HTO and CHK are respectively for input, histograms and outputs of the generator. 
Now it's time to install the software, so `cd $MYDIR`, download **[here](https://box.in2p3.fr/index.php/s/eLK2fj6mxQC6BRN/download/epos4.0.0.tgz)** the EPOS4 software from the official website, or get it from bash using 
```
wget https://box.in2p3.fr/index.php/s/eLK2fj6mxQC6BRN/download/epos${EPOSVSN}.tgz
```
Then extract the content of the downloaded archive `tar xvfz epos${EPOSVSN}.tgz`.

## Compilation and test using DEMO mode

We are going to set the DEMO mode and compile the program without requiring ROOT and FastJet. 

```
export COP=DEMO
cd $EPO
rm -Rf $LIBDIR && cmake -B$LIBDIR && make -C$LIBDIR -j8
```
Now we can simply run the first example provided by the developers
```
cd examples 
$EPO/scripts/epos expl1 
```

## Compilation and run using the BASIC mode

Now it's time to compile the full program with the BASIC mode and be able to run it fully using ROOT and FastJet.  
It is very important that the variables ROOTSYS, FASTSYS and FASTJETSYS are declared in the bash session, otherwise the compilation will fail (issues in Xepos). Furthermore, the environment variables declared **[here](#compilation-and-test-using-demo-mode)**, in the first code box, are mandatory also for this section. 
```
export path=$ROOTSYS/bin:$FASTSYS/bin:$path
export COP=BASIC
export LD_LIBRARY_PATH=$ROOTSYS/lib:$ROOTSYS/lib/root:/usr/local/lib:$LD_LIBRARY_PATH
cd $EPO
rm -Rf $LIBDIR && cmake -B$LIBDIR && make -C$LIBDIR -j8
``` 
If the instructions were followed properly, you should now be able to run all the other examples provided by the developers (expl 2..6).
```
cd examples 
$EPO/scripts/epos expl5
```

In order to obtain trees from EPOS, the program needs to be run with a specific flag and an enabled function in the .optns `fillTree4(C1)`
```
$EPO/scripts/epos -root expl2
```