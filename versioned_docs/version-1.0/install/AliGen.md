---
sidebar_position: 4
---

# Extra: EPOS4 implementation in AliGenerators

EPOS4 is not yet distributed with AliGenerators, but the generator can be installed alongside the framework.  
This was tested by loading AliGenerators on my personal PC, and on LXPLUS and everything is working fine.  
The first thing to do is enter the AliGenerators environment, before proceeding to the rest of this tutorial.  
Now define the environment variable for FASTJET in this way `export FASTSYS=$FASTJET` (you don't need FASTJETSYS in this case).  
The procedure is very similar to what described in the [EPOS4 installation page](./EPOS4.md), but there are some tweaks to be performed in order to make the program work properly. 

One of the most important is to define the compiler that you're going to use in this way
```
export CC=gcc
export CXX=g++
export FC=gfortran
```
otherwise CMAKE is going to pick up the compilers from /usr/bin/, which are very old on LXPLUS and will make the compilation fail (4.8.5 against 10.2 in the latest AliGen version).  
Moving to the folder in which you have the .tgz to be extracted you need to define the environment variables for EPOS
```
export EPOSVSN=4.0.0
export MYDIR=$PWD/
export EPO=${MYDIR}epos${EPOSVSN}/
export LIBDIR=${EPO}bin
export OPT=./   
export HTO=./
export CHK=./
```
and you can untar the archive, by doing `tar xvfz epos${EPOSVSN}.tgz`.  
If you didn't download yet the program, you can do it on bash by executing
```
wget https://box.in2p3.fr/index.php/s/eLK2fj6mxQC6BRN/download/epos${EPOSVSN}.tgz
```
A **fundamental part** of the installation is now to edit yourself, or replace it with **[this](./assets/CMakeLists.txt)**, the CMakeLists.txt file by adding the lines
```
list(APPEND LINK_LIBRARY_LIST
    $ENV{GMP_ROOT}/lib/libgmp.a
    )
list(APPEND LINK_LIBRARY_LIST
    $ENV{CGAL_ROOT}/lib/libCGAL.so
    )    
```
right before `target_link_libraries(Xepos ${LINK_LIBRARY_LIST})`, so that the compiler is able to link properly the mandatory libraries to make the program work. If this is not done, the compilation will return an error on the linker.  
The last point is to declare the last environment variables and to start the compilation by doing 
```
export path=$ROOTSYS/bin:$FASTSYS/bin:$path
export COP=BASIC
cd $EPO
rm -Rf $LIBDIR && cmake3 -B$LIBDIR && make -C$LIBDIR -j8
```
Note that you don't need to setup LD_LIBRARY_PATH in this case, because AliGenerators does it automatically. Moreover, in your personal PC you might need to replace `cmake3` with `cmake`, otherwise you won't be able to proceed with the compilation (you still need CMAKE 3 version however).

If everything was done correctly, now you can move to run your first example, by using the BASIC mode of EPOS which requires both FASTJET and ROOT, unlike the DEMO mode. 
```
cd examples 
$EPO/scripts/epos expl4
```
If you want to obtain a root file, remember to run the program using the flag `-root`.