---
sidebar_position: 4
---

# ROOT tree output

As for many things in the generator, the EPOS4 tree output obtained in the .root files, generated by enabling the `-root` flag, is not documented. Hence a list of the main components that you will find is presented here.  
Mind again that some of these variables are not documented even in the source code, so the real physical meaning might change in the next versions of this tutorial.  
Before starting the list, however, I need to mention that two cloned versions of the same trees are going to be found in the .root file, which are equivalent from my understanding.

## teposhead
- **iversn**  → EPOS version number
- **laproj**  → atomic number projectile
- **maproj**  → mass number projectile
- **latarg**  → atomic number target
- **matarg**  → mass number target
- **engy**    → energy in the cms in GeV
- **nfull**   → number of full events
- **nfreeze** → number of freeze outs per full event (to increase stat)

## teposevent
-  **px** → p_x of particle
-  **py** → p_y of particle
-  **pz** → p_z of particle
-  **e** →  energy of particle
-  **x** →  x component of formation point
-  **y** →  y component of formation point
-  **z** →  z component of formation point
-  **t** →  formation time
-  **id** → EPOS particle ID 
-  **ist** → particle status (hadron last generation (0) or not (1); other numbers refer to partons, Pomerons, etc)  
-  **ity** → type of particle origin (20-29 from soft strings, 30-39 from hard strings, 40-59 from remnants, 60 from fluid)
-  **ior** → index of father (resonance decay products have only a father)
-  **jor** → index of mother (mothers are needed for exemple for strings: the partons between ior and jor constitute the string)
-  **np** → number of particles
-  **bim** → impact parameter (usually other choices are possible

Other observables that you might find in the trees are not documented (or I didn't find them in the source code, sorry if that's the case).