---
sidebar_position: 3
---

# Set random seeds

By default, the script to run EPOS4 uses a fixed seed for the random number initialisation, so running the code several times will produce identical results. To change this, you can replace "seedj=111111111" and "seedi=222222222" in the script by
```
seedj=date '+%N'
seedi=date '+%N'
```
allowing to generate multiple runs and increase the statistics. 