---
sidebar_position: 1
---

# Basic syntax

The generator is run thanks to .optns files that have a specific syntax, whose basis will be explained in the following lines. To do so we're going to look at the examples found in the example installation folder (which are also uploaded in this repository[^1]).   
Commented pieces of code in the .optns files start with "!"

## Setup the run

Three parts are needed in order to setup properly the generator from the option file:
1.  Definition of Run parameters
2.  Analysis
3.  Output 

### Run parameters

The first lines of the .optns file are the generator parameters on the physics to be simulated.  
At the beginning the application must be declared

```
application hadron
```

The application can be for hadron-hadron collisions (as in the example, including also hadron-nucleus and nucleus-nucleus), e+e- (`ee`), or with decay of kinky strings (`kinky`).
I will describe more in detail the case of hadron-hadron collisions, in which the beams need to be defined, including the centre-of-mass energy. The configuration for pp collisions at 7 TeV looks like this

```
set laproj 1 
set maproj 1 
set latarg 1 
set matarg 1 
set ecms 7000
```
where ``laprog, maproj`` and ``latarg, matarg`` are, respectively, the projectile and target atomic and mass number, and ecms is the centre-of-mass energy.  
We can now hardcode the number of events using the function `set nfull 10` to generate for example 10 full events: these include hydrodynamical evolution and other physics processes which are not present if the events are declared such as `set nevent 10`. In addition we can select suppressed decays.

```
nodecays
 110 120
end 
```
Where we're removing for example the decay from the pi^0 and pi^+. Other particles can be added via their EPOS ID (dissimilar from the PDG ones for some reason). A list of them is reported **[here](./tableid)**.  
In addition there are a series of parameter that we can set in this section which will be listed with their most used values in the examples provided by EPOS4 developers and discussed in detail
- `set istmax 25` → the particles have a specific status which is considered for storage, the first digit (left) defines the kind of object, while the second is related to the generation type (0 last generation, 1 otherwise):
    - 40 and 41 : Remnant
    - 30 and 31 : Pomeron
    - 20 and 21 : Parton
    - 10 and 11 : Droplet
    - 00 and 01 : Particle
25 is a commonly used value in the examples (but it's not explained what the 5 should be).
- `set iranphi 1` → this is for internal use. If iranphi = 1 the events will be rotated such that the impact parameter angle and the (n=2) event plane angle (based on string segments) coincide. Particles are rotated back at the end. 
- `ftime on` → non-zero time for the string formation
- `set ninicon 1` → number of events per initial condition, used particularly for hydro evolution. If it's positive then the same impact parameter value is considered, otherwise it's generated in each event.
- `core off` → enable/disable core corona 
    - PFE → Parametrised Fluid Expansion
    - full → full core/corona mechanism
    - off → no core/corona
- `hydro off`→ enable/disable hydrodynamical evolution
    - hlle → hydrodynamic evolution on
    - off → no hydro
- `eos off` → enable/disable Equation of State
    - x3ff → standard EPOS EoS
    - off → no EoS
- `hacas off` → enable/disable hadronic cascade 
    - full → hadronic cascade turned ON
    - off → no hadronic cascade
- `set nfreeze 1` → number of freeze out events per epos event 
- `set modsho 1` → printout every modsho events → This is completely unclear unfortunately. There is no documentation on EPOS regarding what this means. There seems to be two values (1 and 100) which should do exactly the same thing. **To decipher thoroughly**
- `set centrality 0` → Only 0 gives min bias events, otherwise the centrality will be between an initial value (hardcoded in EPOS) and this maximum set value by the user
- `fillTree4(C1)` → used to obtain tree output. It's done in generation phase, not in analysis, so only trees can be returned in output, not histograms/graphs. There are only two possible arguments to provide the function 
    - "C1" is linked to the absolute value of the impact parameter 
    - "C2" is related to the number of real EPOS collisions
As of now (23/11/2022) I question myself on what this could mean... 
- `print * 2` → this prints the particle list in a file .check file inside the CHK folder (I couldn't find the reason why the number must be 2)

### Analysis

EPOS is able to analyse the generated events online, without the need to use other programs or framework.  
This is done in specific block of codes delimited by the tags
```
beginanalysis
    ...
endanalysis        
```
For each analysis block we can define an histogram in which to add our analysis output using specific configurations of particles and trigger. The syntax for the initialization of the histogram is the following
```
histogram
    xvariable
    yvariable
    normalisationcode
    xmin
    xmax
    nbins
```
where `xvariable/yvariable` can be many observables, whose most used ones are
- **numptl** → number of particles
- **eco** → energy in comoving frame
- **tau** → lifetime (?)
- **absrap** → absolute rapidity value
- **rap** → rapidity
- **pt** → transverse momentum
- **phi** → phi angle
- **eta** → pseudorapidity
- **theta** → theta angle
- **pt2** → transverse momentum squared
- **idptl** → particle id on EPOS
- **istptl** → generation flag: last generation (0) or not (1)
- **mass**
- **idaptl** → absolute id of particle on EPOS
- **beta**
- **mu14** → charged particle multiplicity with |eta| \<1 and pt \>0.4
- **delphi** → azimuthal correlation
- **mux** → charged particle multiplicity
- **abseta** → absolute value of pseudorapidity
- **mulevt** → multiplicity, this is an accumulative event variable
- **numevt** → number of events
- **ptevt** → transverse momentum of the event (?), this is an accumulative event variable 

The `normalisationcode` is a factor multiplied or divided to the histogram, and it is a hijk number following the options 
- h  
    - 0: normal
    - 1: accumulated
- i  
    - 0:  * 1
    - 1:  y => y*x
    - 2:  y => y/x/2/pi (modified for mt0)
    - 3:  kno-scaling
    - 4:  y => y/x**1.5
    - 5:  y => y/x
    - 6:  y => y*xi (for conex, xi=x of the bin)
    - 7:  y => y/x/(x-m)    
- j  
    - 0:  * 1
    - 1:  / bin-width
    - 2:  * sigma_total / bin-width
    - 3:  * sigma_diff / bin-width   
- k  
    - 0:  * 1
    - 1:  / number of events
    - 2:  / number of triggered events
    - 4:  / bin-counts
    - 5:  / bin sum
    - 6:  / number of summed bin-counts (yield=1.)
    - 7:  uses same normalization as one histo before
By default the normalisation number is 0000 that can be writte directly as 0 (or omitted). If we want to normalise per number of events and bin-width we could use the code 0011, or simply 11 (as done in multiple examples provided by the developers).  
`xmin, xmax` and `nbins` are straighforward, so they won't be explained.

After the histogram definition, we need to declare the filling condition and we can do that by using the `trigger` function. The same variables used for filling the histograms can be used for the triggering (such as rap and eta), apart form the cumulative event quantities.  
The function can be used in three modes:
- Single: only one option 
`trigger var minval maxval`  
Example
```trigger rap -1 1```
- Or: one of the conditions must be valid
`trigger or nvar var0 min0 max0 ... varn minn maxn`
as in   
`trigger or 2 rap -1 1 eta 0 1`
- And: all conditions must be true
`trigger contr nvar var0 min0 max0 ... varn minn maxn`

In the end you can add the idcodes of the particles to be considered for the analysis, using the EPOS ID codes described earlier. For protons and antiprotons we would add for examples the string `idcode 1120  idcode -1120`. From the example files we also discover that the code for charged particles is `9970`.  
Another option that we can consider adding is a factor to apply to the whole histogram, which is done by using the `hisfac` parameter. If we want to divide for 2 for example we could simply add
```
set hisfac 0.5
```
In addition the binning can be set to logarithmic or linear by writing respectively `binning log` or `binning lin` right before the histogram definition.  
The reference frame can also be edited putting after `beginanalysis` the parameter:
```
frame val
```
where **val** can have the values: `total, nucleon-nucleon, target, gamma-nucleon, lab, breit, thrust and spherocity`. Unfortunately there's barely documentation in the source code about these frame, but from the examples it is clear that `total` is the default one, while `thrust` is a particular frame used in e+e- collisions.  
There are many more things that were omitted, but the lack of proper documentation makes it difficult to understand all the settings included in the source code by the developers. 

### Output 

After the definition of every single histogram you need to add three simple lines to be able to write the final results to output file. The histograms will be inserted in a .histo file inside the $HTO folder.
```
write "histoname"
histoweight 
writearray 3 
```
These three lines will generate an histogram called histoname, will provide the event weight for the histogram and create a 3 column table in the .histo file, where the columns are, from left to right, x, y, and the error on y. 

## Example 

Implementing all the things we learned in this page, we can finally write our first example (you can download it directly **[here](./assets/examplepp.optns)**), where we are going to collide protons with a 7 TeV centre-of-mass energy. We are going to obtain the pt spectrum of charged pions and rapidity distribution of protons and antiprotons, plotting the results in a .histo file which we can later analyse using ROOT or the software we like. 
```jsx title="examplepp.optns"
!---------------------------------------
!            Run Definition
!---------------------------------------
application hadron                                         
set laproj 1 
set maproj 1 
set latarg 1 
set matarg 1 
set ecms 7000 
set istmax 25 
set iranphi 1 
ftime on     

set ninicon 1            !number of initial conditions used for hydro evolution
core off                 !core/corona not activated
hydro off                !hydro not activated
eos off                  !eos not activated
hacas off                !hadronic cascade not activated  
set nfull 200            !number of events
set nfreeze 1            !number of freeze out events per hydro event 
set modsho 1             !printout every modsho events
set centrality 0         !0=min bias 

!----------------------------------------------------------
!           Analysis definition (first histogram)
!----------------------------------------------------------

beginanalysis  
  histogram 
    pt     !x = transverse momentum
    numptl !y = number of particles
    11     !normalisation code (per event, per bin width) see below
    0      !x-min 
    5      !x-max
    10     !number of bins
  trigger rap -1 1  !rapidity range
  set hisfac 0.5    !divide by rapidity range
  idcode 120  idcode -120  !charged pions
endanalysis

!---------------------------------------------------
!               First histogram output
!---------------------------------------------------

write "Charged pi distro pt"
histoweight 
writearray 3   

!----------------------------------------------------------
!           Analysis definition (second histogram)
!----------------------------------------------------------

beginanalysis  
  histogram 
    rap    !x = rapidity
    numptl !y = number of particles
    11     !normalisation
    -5     !x-min 
    5      !x-max
    20     !number of bins
  idcode 1120  idcode -1120  !protons and antiprotons
endanalysis

!---------------------------------------------------
!               Second histogram output
!---------------------------------------------------

write "Proton-antiproton rapidity distribution"
histoweight 
writearray 3   
```
This task can be run using the instructions you can read in the **[next section](./Start)** of this tutorial.






















[^1]: [expl1.optns](./assets/expl1.optns), [expl2.optns](./assets/expl2.optns), [expl3.optns](./assets/expl3.optns), [expl4.optns](./assets/expl4.optns), [expl5.optns](./assets/expl5.optns), [expl6.optns](./assets/expl6.optns)