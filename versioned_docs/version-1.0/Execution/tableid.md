---
sidebar_position: 3
---

# Table particle ID

## Quarks, Leptons and photon

| Name | ID |
|------|------|
|u quark | 1 |
|d quark | 2 |
|s quark | 3 |
|c quark | 4 |
|b quark | 5 |
|t quark | 6 |
|electron neutrino | 11 |
|electron | 12 |
|muon neutrino | 13 |
|muon | 14 |
|tau neutrino | 15 |
|tau | 16 |
|gluon | 9 |
|photon | 10 |
|KS | 20 |
|KL | -20 |
|W+ | 80 |
|Z0 | 90 |

## Basic hadrons

### Mesons

| ID | Name | Mass | Charge |
|------|------|------------|-------|
| 110 | PI0 | 0.13496 | 0.00 |
| 120 | PI+ | 0.13957 | 1.00 |
| -120 | PI- | 0.13957 | -1.00 |
| 220 | ETA | 0.54745 | 0.00 |
| 130 | K+ | 0.49367 | 1.00 |
| -130 | K-| 0.49367 | -1.00 |
| 230 | K0 | 0.49767 | 0.00 |
| -230 | AK0 | 0.49767 | 0.00 |
| 330 | ETAP | 0.95760 | 0.00 |
| 140 | AD0 | 0.18645E+01 | 0.00 |
| -140 | D0 | 0.18645E+01 | 0.00 |
| 240 | D- | 0.18693E+01 | -1.00 |
| -240 | D+ | 0.18693E+01 | 1.00 |
| 340 | F- | 0.19688E+01 | -1.00 |
| -340 | F+ | 0.19688E+01 | 1.00 |
| 440 | ETAC | 0.29788E+01 | 0.00 |
| 150 | B+ | 0.5279E+01 | 1.00 |
| -150 | B- | 0.5279E+01 | -1.00 |
| 250 | B0 | 0.5279E+01 | 0.00 |
| -250 | AB0 | 0.5279E+01 | 0.00 |
| 350 | B0S | 0.5369E+01 | 0.00 |
| -350 | AB0S | 0.5369E+01 | 0.00 |
| 111 | RHO0 | 0.76810 | 0.00 |
| 121 | RHO+ | 0.76810 | 1.00 |
| -121 | RHO- | 0.76810 | -1.00 |
| 221 | OMEG | 0.78195 | 0.00 |
| 131 | K*+ | 0.89159 | 1.00 |
| -131 | K*- | 0.89159 | -1.00 |
| 231 | K*0 | 0.89610 | 0.00 |
| -231 | AK*0 | 0.89610 | 0.00 |
| 331 | PHI | 0.10194E+01 | 0.00 |
| 141 | AD*0 | 0.20071E+01 | 0.00 |
| -141 | D*0 | 0.20071E+01 | 0.00 |
| 241 | D*- | 0.20101E+01 | -1.00 |
| -241 | D*+ | 0.20101E+01 | 1.00 |
| 341 | F*- | 0.21103E+01 | -1.00 |
| -341 | F*+ | 0.21103E+01 | 1.00 |
| 441 | JPSI | 0.30969E+0u quark

### Baryons

| ID | Name | Mass | Charge |
|------|------|------------|-------|
| 1120 | P | .93828E+00 | 1.00 |
| 1220 | N | .93957E+00 | 0.00 |
| 1130 | S+ | .11894E+01 | 1.00 |
| 1230 | S0 | .11925E+01 | 0.00 |
| 2130 | L | .11156E+01 | 0.00 |
| 2230 | S- | .11974E+01 | -1.00 |
| 1330 | XI0 | .13149E+01 | 0.00 |
| 2330 | XI- | .13213E+01 | -1.00 |
| 1140 | SC++ | .24527E+01 | 2.00 |
| 1240 | SC+ | .24529E+01 | 1.00 |
| 2140 | LC+ | .22849E+01 | 1.00 |
| 2240 | SC0 | .24525E+01 | 0.00 |
| 1340 | USC. | .25000E+01 | 1.00 |
| 3140 | SUC. | .24000E+01 | 1.00 |
| 2340 | DSC. | .25000E+01 | 0.00 |
| 3240 | SDC. | .24000E+01 | 0.00 |
| 3340 | SSC. | .26000E+01 | 0.00 |
| 1440 | UCC. | .35500E+01 | 2.00 |
| 2440 | DCC. | .35500E+01 | 1.00 |
| 3440 | SCC. | .37000E+01 | 1.00 |
| 1111 | DL++ | .12320E+01 | 2.00 |
| 1121 | DL+ | .12320E+01 | 1.00 |
| 1221 | DL0 | .12320E+01 | 0.00 |
| 2221 | DL- | .12320E+01 | -1.00 |
| 1131 | S*+ | .13823E+01 | 1.00 |
| 1231 | S*0 | .13820E+01 | 0.00 |
| 2231 | S*- | .13875E+01 | -1.00 |
| 1331 | XI*0 | .15318E+01 | 0.00 |
| 2331 | XI*- | .15350E+01 | -1.00 |
| 3331 | OM- | .16722E+01 | -1.00 |
| 1141 | UUC* | .26300E+01 | 2.00 |
| 1241 | UDC* | .26300E+01 | 1.00 |
| 2241 | DDC* | .26300E+01 | 0.00 |
| 1341 | USC* | .27000E+01 | 1.00 |
| 2341 | DSC* | .27000E+01 | 0.00 |
| 3341 | SSC* | .28000E+01 | 0.00 |
| 1441 | UCC* | .37500E+01 | 2.00 |
| 2441 | DCC* | .37500E+01 | 1.00 |
| 3441 | SCC* | .39000E+01 | 1.00 |
| 4441 | CCC* | .48000E+01 | 2.00 |

## Higher Resonances

| id | label |
|------|------------|
| 332 | F0(975)|
| 112 | A0(980)|
| 122 | A+(980)|
| 1112 | DL++(1620) |
| 1113 | DL++(1700) |
| 1114 | DL++(1925) |
| 2222 | DL-(1620) |
| 2223 | DL-(1700) |
| 2224 | DL-(1925) |
| 1122 | N*+(1440) |
| 1123 | N*+(1530) |
| 1124 | DL+(1620) |
| 1125 | N*+(1665) |
| 1126 | DL+(1700) |
| 1127 | N*+(1710) |
| 1128 | DL+(1925) |
| 1222 | N*0(1440) |
| 1223 | N*0(1530) |
| 1224 | DL0(1620) |
| 1225 | N*0(1665) |
| 1226 | DL0(1700) |
| 1227 | N*0(1710) |
| 1228 | DL0(1925) |
| 1233 | L(1405)|
| 1234 | L(1520)|
| 1235 | L(1645)|
| 1236 | S0(1665)|
| 1237 | S0(1776)|
| 1238 | L(1845)|
| 1239 | S0(1930)|
| 1132 | S+(1665)|
| 1133 | S+(1776)|
| 1134 | S+(1930)|
| 2232 | S-(1665)|
| 2233 | S-(1776)|
| 2234 | S-(1930)|

## ID Codes of Exotic Particles

ID codes of exotic particles, referring to objects
outside the resonance table (strings or droplets) are meant to characterize the quark content,
and not, as for the above particles, also the mass (or spin).  
The general code consists of a 9 digit integer, the first onebeing the number "8". the next 4 digits represent the numbers of u quarks, d quarks, s quarks, and c quarks. the following 4 digits represent the corresponding numbers of antiquarks. For example, 833000000 represents an object with
3 u quarks and 3 d quarks; 800303000 represents an object with 3 s quarks and 3 u antiquarks.

When for (at least) one flavor there are more than 9 (anti)quarks, one uses again a 9 digits code, with the first digit  being the number "7". The following 4 digits have to be considered as a four digit number representing the number of quarks, and the last four digits represent correspondingly the number of antiquarks. For example, 700270000 represents an object with 27 quarks; 701210013 represents an object with 121
quarks and 13 antiquarks.


