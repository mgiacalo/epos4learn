---
sidebar_position: 1
---

# Introduction

This tutorial will teach you how to install properly all the packages required to run EPOS4 with all the 
examples provided by the developers of the generator. For the tutorial, Ubuntu 20.04 (including WSL2) has been used, 
but similar commands can be adapted to other operating systems. More documentation can be found in the [EPOS4 official website](https://klaus.pages.in2p3.fr/epos4/).  
As of 22/11/2022, **the compilation of EPOS fails on Apple** machines (error due to fortran using large static arrays). An email has been sent to the developers on the matter.

This tutorial won't help you understand the physics behind the model, but it will simply guide you on how to run your first simulations. An in-depth description of the theoretical basis of the generator is provided in **[this file](./asset/KlausWernerWorkshop2022EPOS4.pdf)**, which comes from a MC Workshop done by Klaus Werner in July 2022.

**Enjoy!**