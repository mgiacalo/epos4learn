---
sidebar_position: 4
---

# HEPMC + RIVET

## Usage on standalone installation

As opposed to the previous versions of the generator, EPOS4 supports natively the HEPMC output.  
To generate it, you need to enable the parameter
```
set ihepmc 1
```
in the option file (before the analysis declaration), and provide the `-hepmc` flag before the option filename when running the `epos` script. By default the previous parameter is set to 0, which disables the output in this format, in spite of using or not the flag.  
From the source code, it seems that the only other option that can be set regarding the HEPMC formatting is the definition of the reference system of the collision through the `ihepframe` parameter whose value is by default 0 (centre-of-mass), and can be changed to 1 to use the Lab reference system.  
The file created will be named z-**optnfilename**.hepmc, which can be used directly in RIVET (tested on LXPLUS using an edited version of expl5.optns following the tutorial and the ALICE_2019_I1735351 rivetisation in pp collisions at 13 TeV).

## AliGenerators implementation

The same procedure can be followed on AliGenerators, but this is true only up to the version `EPOS4::v4.0.0-alice1` (`VO_ALICE@AliGenerators::v20221209-1`). In the latest tag, which is `EPOS4::v4.0.0-alice2`, both the source code of EPOS4 and the script to run the generator were edited in order to allow the user to rename the hepmc file. This was an important addition in order to make the generator run on the GRID and with FIFOs.  
The syntax is very simple: you can add the same `-hepmc` flag before the option filename, providing or not a filename after it. If a filename is not provided the default hepmc output will be named `z-HepMC.hepmc`, otherwise 
the file will be named as `filename.hepmc`.  
**TWO IMPORTANT POINTS:**
1. The filename provided as an argument must not contain the extension, e.g.
    ```
    ./epos -hepmc TEST expl5
    ```
    will create an HepMC output named TEST.hepmc.
2. If you want to use a FIFO, you need to remember to create it BEFORE giving the filename as a parameter to the script, otherwise a normal .hepmc file will be used.

This part is still a work in progress, since the GRID implementation is not ready yet for users. In spite of this, you can still test everything on your local installation of AliGenerators, or using LXPLUS.

