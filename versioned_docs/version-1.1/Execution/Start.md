---
sidebar_position: 2
---

# Start the simulation

Before running any example, or user-make task, we need to be sure that the environment variables used by EPOS4 are defined correctly. In order to make it easier for the user, they are listed here, without the need to check them again the previous part of the tutorial (mind that you need to `cd` in the folder containing the epos4.0.0 one, not directly inside it).
```
export EPOSVSN=4.0.0
export MYDIR=$PWD/
export EPO=${MYDIR}epos${EPOSVSN}/
export LIBDIR=${EPO}bin
export OPT=./   
export HTO=./
export CHK=./
export FASTSYS=`fastjet-config --prefix`
export path=$ROOTSYS/bin:$FASTSYS/bin:$path
export COP=BASIC
export LD_LIBRARY_PATH=$ROOTSYS/lib:$ROOTSYS/lib/root:/usr/local/lib:$LD_LIBRARY_PATH #not to do in case we are in aligenerators
cd $EPO
```
If everything was done correctly we should now be inside the epos$\{EPOSVSN\} folder. From here we can copy the **[examplepp.optns](./assets/examplepp.optns)** file we used before (for example) inside the examples folder (just to have it in one place, you can run it from wherever you prefere) and start our first example by doing
```
cd examples
$EPO/scripts/epos examplepp #.optns is omitted
```
EPOS4 should run succesfully and provide you in the end with a z-examplepp.histo file, containing the declared histograms and the used weights.

Another example might run done using the **[examplePbPb.optns](./assets/examplePbPb.optns)** file with lead-lead collisions at 5.02 TeV, which contains the same histograms as the pp option file. It is uploaded here just for reference in case someone wants to tweak it and generate their own analysis. Core/corona mechanism, hadronic cascade, Equation-Of-State and hydrodynamics are all enabled in this last one.  
In addition, the ROOT functionalities are also enabled, so we can obtain the trees coming from this example by using the `-root` flag.
```
$EPO/scripts/epos -root examplePbPb
```
For a short explanation on the tree output from EPOS4, you can refer to the **[ROOT ID](./rootid)** page.