---
sidebar_position: 2
---

# FastJet

This software is mandatory to use all the functionalities of EPOS4.

The installation is straightforward and all the steps can be found in the **[FastJet](https://fastjet.fr/quickstart.html)** official website and will be reported in the next lines.
First download code and unpack it (you can either use "curl -O" or "wget")

```
curl -O http://fastjet.fr/repo/fastjet-3.4.0.tar.gz
tar zxvf fastjet-3.4.0.tar.gz
cd fastjet-3.4.0/
```

Proceed then to the installation of the software
```
./configure --prefix=$PWD/../fastjet-install
make
make check
make install
cd ..
```

To test if the installation was successful, **[download this example program](./assets/FJexample.cc)**, compile it and run it using the command
```
g++ FJexample.cc -o FJexample \
      `fastjet-install/bin/fastjet-config --cxxflags --libs --plugins`
./FJexample.cc
```
which should return an output consisting of the FastJet banner, and the following lines
```
Clustering with Longitudinally invariant anti-kt algorithm with R = 0.7 
and E scheme recombination
        pt y phi
jet 0: 103 0 0
    constituent 0's pt: 99.0001
    constituent 1's pt: 4.00125
jet 1: 99 0 3.14159
    constituent 0's pt: 99
```

The final step in order to run EPOS4 properly is to declare the variables `FASTJETSYS` and `FASTSYS`[^1], by simply executing 
```
export FASTJETSYS=`fastjet-install/bin/fastjet-config --prefix`
export FASTSYS=$FASTJETSYS
```

[^1]: They both need to be declared, otherwise there will be compilation errors with EPOS4 in BASIC mode. 