import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Easy to Use',
    Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        This tutorial was designed to guide users from the installation to the execution of their first simulations.
      </>
    ),
  },
  {
    title: 'The icon is a dinosaur!',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        This is probably already the best tutorial you've ever read. Come on, the mascot is a DINOSAUR!
      </>
    ),
  },
  {
    title: 'Email me for changes',
    Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        You are the best way to improve the tutorial. Don't be shy and email me at mgiacalo@cern.ch.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
